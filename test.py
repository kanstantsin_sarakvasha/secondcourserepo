import unittest
from functions import *

class Testadd(unittest.TestCase):

    def test_equal(self):
        self.assertEqual(add(2,2), 4)

    # uncomment and push to see pipeline job fail
    # def test_fail(self):
    #     self.assertEqual(add(2,2), 5)

    def test_false(self):
        self.assertNotEqual(add(2,2), 8)

if __name__ == '__main__':
    unittest.main()